CREATE TABLE IF NOT EXISTS country
(
    id   binary(255)  not null,
    name varchar(255) not null,
    primary key (id)
);

CREATE TABLE IF NOT EXISTS hotel
(
    id         binary(255)  not null,
    name       varchar(255) not null,
    country_id binary(255)  not null,
    primary key (id),
    foreign key (country_id)
        references country (id)
);

CREATE TABLE IF NOT EXISTS tour
(
    id   binary(255)  not null,
    name varchar(255) not null,
    primary key (id)
);

CREATE TABLE IF NOT EXISTS reservation
(
    id      binary(255)  not null,
    number  varchar(255) not null,
    tour_id binary(255)  not null,
    user_id binary(255)  not null,
    price   double,
    primary key (id),
    foreign key (tour_id)
        references tour (id),
    foreign key (user_id)
        references customer (id)
);

CREATE TABLE IF NOT EXISTS customer
(
    id           binary(255) not null,
    email        varchar(255),
    name         varchar(255),
    phone_number varchar(255),
    primary key (id)
);

CREATE TABLE IF NOT EXISTS review
(
    id       binary(255)  not null,
    name     varchar(255) not null,
    user_id  binary(255)  not null,
    hotel_id binary(255)  not null,
    primary key (id),
    foreign key (user_id)
        references customer (id),
    foreign key (hotel_id)
        references hotel (id)
);

CREATE TABLE IF NOT EXISTS hotel_tour
(
    hotel_id binary(255) not null,
    tour_id  binary(255) not null,
    primary key (hotel_id, tour_id),
    foreign key (hotel_id)
        references hotel (id) on delete restrict,
    foreign key (tour_id)
        references tour (id) on delete cascade
);

CREATE INDEX username_index ON customer (name);