CREATE TABLE IF NOT EXISTS countries
(
    id   uuid         not null,
    name varchar(255) not null,
    primary key (id)
);

CREATE TABLE IF NOT EXISTS hotels
(
    id         uuid         not null,
    name       varchar(255) not null,
    country_id uuid         not null,
    primary key (id),
    foreign key (country_id)
        references countries (id)
);

CREATE TABLE IF NOT EXISTS tours
(
    id   uuid         not null,
    name varchar(255) not null,
    primary key (id)
);

CREATE TABLE IF NOT EXISTS orders
(
    id      uuid         not null,
    number  varchar(255) not null,
    tour_id uuid         not null,
    user_id uuid         not null,
    price   money,
    primary key (id),
    foreign key (tour_id)
        references tours (id),
    foreign key (user_id)
        references users (id)
);

CREATE TABLE IF NOT EXISTS users
(
    id           uuid not null,
    email        varchar(255),
    name         varchar(255),
    phone_number varchar(255),
    primary key (id)
);

CREATE TABLE IF NOT EXISTS reviews
(
    id       uuid         not null,
    name     varchar(255) not null,
    user_id  uuid         not null,
    hotel_id uuid         not null,
    primary key (id),
    foreign key (user_id)
        references users (id),
    foreign key (hotel_id)
        references hotels (id)
);

CREATE TABLE IF NOT EXISTS hotel_tour
(
    hotel_id uuid not null,
    tour_id  uuid not null,
    primary key (hotel_id, tour_id),
    foreign key (hotel_id)
        references hotels (id) on delete restrict,
    foreign key (tour_id)
        references tours (id) on delete cascade
);

CREATE INDEX username_index ON users (name);
